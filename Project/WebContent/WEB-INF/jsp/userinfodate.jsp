<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
<title>userinfodate</title>
</head>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body>
<div class="container-fluid">
	<br>
	<p class="title text-center f">_CHEST_</p>

	<div class="col s8 offset-s2">
		<p class="text-right">
		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

			<%if(isLogin){ %>
				<a href="UserData"><i class="btn btn-dark"><span class="material-icons">account_box</span></i></a>
			<%}else{ %>
				<a href="SignupServlet"><i class="btn btn-dark"><span class="material-icons">add</span></i></a>
			<%} %>

			<a href="CartServlet"><i class="btn btn-dark"><span class="material-icons">shopping_cart</span></i></a>

			<%if(isLogin){ %>
			<a href="LogoutServlet"><i class="btn btn-dark"><span class="material-icons">launch</span></i></a>
			<%}else{ %>
			<a href="LoginComfirm"><i class="btn btn-dark"><span class="material-icons">vpn_key</span></i></a>
			<%} %>
		</p>
	</div>
<div>
	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	 			<a class="nav-link" href="Info">INFO</a></li>
	 		<li class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
					CATEGORY<span class="caret"></span></a>
			 <div class="dropdown-menu">
				<a class="dropdown-item" href="Category?id=1">青年・少年</a>
				<a class="dropdown-item" href="Category?id=2">恋愛</a>
				<a class="dropdown-item" href="Category?id=3">ファンタジー</a>
				<a class="dropdown-item" href="Category?id=4">その他</a>
			</div>
			</li>
			</ul>
		</div>
	</nav>
</div>

		<p class="text-center">ログイン時に送った要望メッセージアーカイブ</p>

		<div class="container">
		<br>
			<table class="table" style="table-layout:fixed;width:100%;">
			  <thead>
				<tr>
				  <th scope="col">日付</th>
				  <th scope="col">内容</th>
				</tr>
			  </thead>
			  <tbody>
			  <c:forEach var="info" items="${infoList}">
				<tr>
				  <td>${info.createDate}</td>
				  <td>${info.detail}</td>
				  </tr>
				</c:forEach>
			  </tbody>
			</table>

<div class="progress">
  <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>


			<p class="title text-center">
			<a href="UserData" class="btn btn-outline-dark">ユーザ画面へ
			<span class="material-icons">playlist_play</span>
			</a></p>
		</div>




<div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
				<a href="TopServlet"><i class="btn btn-link">
					<span class="material-icons">arrow_upward</span></i></a></li>
		</ul>
		</div>
	</nav>
</div>


</div>
</body>
</html>