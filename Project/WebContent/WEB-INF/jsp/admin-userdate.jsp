<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
<title>admin-userdate</title>
</head>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body>
<div class="container-fluid">
	<br>
	<p class="title text-center f">_CHEST_</p>

	<div class="col s8 offset-s2">
		<p class="text-right">
		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

			<%if(isLogin){ %>
				<a href="UserData"><i class="btn btn-dark"><span class="material-icons">account_box</span></i></a>
			<%}else{ %>
				<a href="SignupServlet"><i class="btn btn-dark"><span class="material-icons">add</span></i></a>
			<%} %>

			<a href="CartServlet"><i class="btn btn-dark"><span class="material-icons">shopping_cart</span></i></a>

			<%if(isLogin){ %>
			<a href="LogoutServlet"><i class="btn btn-dark"><span class="material-icons">launch</span></i></a>
			<%}else{ %>
			<a href="LoginComfirm"><i class="btn btn-dark"><span class="material-icons">vpn_key</span></i></a>
			<%} %>
		</p>
	</div>
<div>
	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	 			<a class="nav-link" href="Info">INFO</a></li>
	 		<li class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
					CATEGORY<span class="caret"></span></a>
			 <div class="dropdown-menu">
				<a class="dropdown-item" href="Category?id=1">青年・少年</a>
				<a class="dropdown-item" href="Category?id=2">恋愛</a>
				<a class="dropdown-item" href="Category?id=3">ファンタジー</a>
				<a class="dropdown-item" href="Category?id=4">その他</a>
			</div>
			</li>
			</ul>
		</div>
	</nav>
</div>


		<p class="text-center">管理人  ユーザ詳細</p>

		<div class="container">
		<br>
			<table class="table" style="table-layout:fixed;width:100%;">
			  <thead>
				<tr>
				  <th scope="col">ID</th>
				  <th scope="col">ログインID</th>
				  <th scope="col">ユーザ名</th>
				  <th scope="col">住所</th>
				  <th scope="col"></th>
				</tr>
			  </thead>
			  <tbody>
			  <c:forEach var="udba" items="${udbaList}">
				<tr>
				  <th scope="row">${udba.id}</th>
				  <td>${udba.loginId}</td>
				  <td>${udba.name}</td>
				  <td>${udba.address}</td>
				  <td></td>
				  </tr>
				  <tr>
				  <td>パスワード</td>
				  <td>${udba.password}</td>
				  <td></td>
				  <td></td>
				  <td>
					<a href="AuserdateUp?id=${udba.id}" class="btn btn-info">更新</a>
					<a href="UserDeleteServlet?id=${udba.id}" class="btn btn-danger">削除</a></td>
				</tr>
				</c:forEach>
			  </tbody>
			</table>
<div class="progress">
  <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>
			<table class="table table-dark" style="table-layout:fixed;width:100%;">
			  <thead>
				<tr>
				  <th scope="col">ID</th>
				  <th scope="col">User_ID</th>
				  <th scope="col">内容</th>
				  <th scope="col">時間</th>
				</tr>
			  </thead>
			  <tbody>
			  <c:forEach var="info" items="${infoList}">
				<tr>
				  <th scope="row">${info.id}</th>
				  <td>${info.userId}</td>
				  <td>${info.detail}</td>
				  <td>${info.createDate}</td>
				  </tr>
				</c:forEach>
			  </tbody>
			</table>

<div class="progress">
  <div class="progress-bar bg-secondary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
</div>


			<p class="title text-center">
			<a href="AdminSDate" class="btn btn-outline-dark">商品画面へ
			<span class="material-icons">playlist_play</span>
			</a></p>
		</div>




<div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
				<a href="TopServlet"><i class="btn btn-link">
					<span class="material-icons">arrow_upward</span></i></a></li>
		</ul>
		</div>
	</nav>
</div>


</div>
</body>
</html>
