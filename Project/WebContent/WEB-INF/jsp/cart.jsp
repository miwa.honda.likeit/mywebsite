<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
<title>cart</title>
</head>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body>
<div class="container-fluid">
	<br>
	<p class="title text-center f">_CHEST_</p>

	<div class="col s8 offset-s2">
		<p class="text-right">
		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

			<%if(isLogin){ %>
				<a href="UserData"><i class="btn btn-dark"><span class="material-icons">account_box</span></i></a>
			<%}else{ %>
				<a href="SignupServlet"><i class="btn btn-dark"><span class="material-icons">add</span></i></a>
			<%} %>

			<a href="CartServlet"><i class="btn btn-dark"><span class="material-icons">shopping_cart</span></i></a>

			<%if(isLogin){ %>
			<a href="LogoutServlet"><i class="btn btn-dark"><span class="material-icons">launch</span></i></a>
			<%}else{ %>
			<a href="LoginComfirm"><i class="btn btn-dark"><span class="material-icons">vpn_key</span></i></a>
			<%} %>
		</p>
	</div>
	<div>
	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	 			<a class="nav-link" href="Info">INFO</a></li>
	 		<li class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
					CATEGORY<span class="caret"></span></a>
			 <div class="dropdown-menu">
				<a class="dropdown-item" href="Category?id=1">青年・少年</a>
				<a class="dropdown-item" href="Category?id=2">恋愛</a>
				<a class="dropdown-item" href="Category?id=3">ファンタジー</a>
				<a class="dropdown-item" href="Category?id=4">その他</a>
			</div>
			</li>
			</ul>
		</div>
	</nav>
</div>

	<div>
		<p class="text-center">C A R T</p>
		<p class="text-center">${cartActionMessage}</p>

		<div class="container">
		<form action="ItemInCartDelete" method="POST">
<div class="text-center">
		<button class="btn btn-outline-dark " type="submit">削除の際はチェックをしてから押してください
			<i class="material-icons right">delete</i></button>

			<div class="row col">
			<c:forEach var="cartitem" items="${cart}" varStatus="status">
				<div class="sample col-md-2">
				<div class="thumbnail text-center">

				<br>
					<!--  <a href="Item?item_id=${cartitem.id}">-->
						<img src="img/${cartitem.fileName}" class="thumbnail">
					<span class="d-inline-block text-truncate" style="max-width: 150px;">
					<br>${cartitem.name}</span><br>${cartitem.author}<br>¥${cartitem.price}
					<input type="checkbox" id="${status.index}" name="delete_item_id_list" value="${cartitem.id}" />
					 <label for="${status.index}">削除</label>
					</div>
					</div>
					<c:if test="${(status.index+1) % 6 == 0 }">
				</div>
				<div class="row">
					</c:if>
					</c:forEach>

			</div>
					</div>
					</form>


	<!--リスト　商品名　配達種類選択して購入確認画面--->
	<br>
	<form action="BuyServlet" method="get">
<table class="table">
  <thead>
    <tr>
      <th scope="col">商品</th>
      <th scope="col">価格(税抜)</th>
    </tr>
  </thead>
  <tbody>
  <c:forEach var="item" items="${cart}" varStatus="status">
    <tr>
      <th scope="row"><a href="SDateServlet?item_id=${item.id}">${item.name}</a></th>
      <td>¥${item.price}</td>
  	</tr>
  	</c:forEach>
	<tr>
      <th scope="row"><p class="text-right">配送方法を必ずお選びください⇨</th>
      <td>
      <div>

		<select class="form-control" name="delivery_method_id">
			<c:forEach var="dmdb" items="${dmdbList}" >
				<option value="${dmdb.id}">${dmdb.name}</option>
			</c:forEach>
		</select>
		</div>
	</td>
    </tr>
  </tbody>
</table>

		<button type="submit" class="btn btn-secondary btn-block">購入確認</button>
		</form>

	</div>



<div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
				<a href="TopServlet"><i class="btn btn-link">
					<span class="material-icons">arrow_upward</span></i></a></li>
		</ul>
		</div>
	</nav>
</div>







</div>
</div>
</body>
</html>
