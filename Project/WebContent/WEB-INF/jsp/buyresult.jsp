<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada" rel="stylesheet">
<title>buyresult</title>
</head>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body>
<div class="container-fluid">
	<br>
	<p class="title text-center f">_CHEST_</p>

	<div class="col s8 offset-s2">
		<p class="text-right">
		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

			<%if(isLogin){ %>
				<a href="UserData"><i class="btn btn-dark"><span class="material-icons">account_box</span></i></a>
			<%}else{ %>
				<a href="SignupServlet"><i class="btn btn-dark"><span class="material-icons">add</span></i></a>
			<%} %>

			<a href="CartServlet"><i class="btn btn-dark"><span class="material-icons">shopping_cart</span></i></a>

			<%if(isLogin){ %>
			<a href="LogoutServlet"><i class="btn btn-dark"><span class="material-icons">launch</span></i></a>
			<%}else{ %>
			<a href="LoginComfirm"><i class="btn btn-dark"><span class="material-icons">vpn_key</span></i></a>
			<%} %>
		</p>
	</div>
<div>
	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	 			<a class="nav-link" href="Info">INFO</a></li>
	 		<li class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
					CATEGORY<span class="caret"></span></a>
			 <div class="dropdown-menu">
				<a class="dropdown-item" href="Category?id=1">青年・少年</a>
				<a class="dropdown-item" href="Category?id=2">恋愛</a>
				<a class="dropdown-item" href="Category?id=3">ファンタジー</a>
				<a class="dropdown-item" href="Category?id=4">その他</a>
			</div>
			</li>
			</ul>
		</div>
	</nav>
</div>

	<div>
		<p class="text-center">購入が完了しました</p>
		<br>
		<p class="text-center">購入詳細</p>

		<div class="container">
			<br>
			<table class="table">
				<thead>
				<tr>
				  <th scope="col">購入日時</th>
				  <th scope="col">配送方法</th>
				  <th scope="col">合計(税込)</th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
				  <th scope="row">${resultBDB.formatDate}</th>
				  <td>${resultBDB.deliveryMethodName}</td>
				  <td>¥${resultBDB.formatTotalPrice}</td>
				</tr>
				</tbody>
			</table>

		　<br>
			<table class="table">

			  <thead class="thead-light">
				<tr>
				  <th scope="col">商品</th>
				  <th scope="col">価格</th>
				  <th scope="col">小計</th>
				</tr>
			  </thead>
			  <tbody>
			  <c:forEach var="buyIDB" items="${buyIDBList}" >
				<tr>
				  <th scope="row">${buyIDB.name}</th>
				  <td>¥${buyIDB.formatPrice}</td>
				  <td>¥${buyIDB.formatPrice}</td>
				</tr>
				</c:forEach>
				<tr>
				  <th scope="row">${resultBDB.deliveryMethodName}</th>
				  <td><p class="text-right"></td>
				  <td>配送料 ¥${resultBDB.deliveryMethodPrice}</td>
				</tr>
			  </tbody>
			</table>

		<p class="title text-center">
			<a href="TopServlet" class="btn btn-secondary">引き続き買い物を続ける</a>
			<a href="UserData" class="btn btn-secondary">ユーザ情報へ</a>
			</p>

			</div>
			</div>





<div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
				<a href="TopServlet"><i class="btn btn-link">
					<span class="material-icons">arrow_upward</span></i></a></li>
		</ul>
		</div>
	</nav>
</div>






</div>
</body>
</html>
