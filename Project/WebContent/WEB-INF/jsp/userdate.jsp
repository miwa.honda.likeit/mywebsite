<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Montserrat+Subrayada"
	rel="stylesheet">
<title>userdate</title>
</head>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body>
	<div class="container-fluid">
		<br>
		<p class="title text-center f">_CHEST_</p>

		<div class="col s8 offset-s2">
			<p class="text-right">
		<% boolean isLogin = session.getAttribute("isLogin")!=null?(boolean) session.getAttribute("isLogin"):false; %>

			<%if(isLogin){ %>
				<a href="UserData"><i class="btn btn-dark"><span class="material-icons">account_box</span></i></a>
			<%}else{ %>
				<a href="SignupServlet"><i class="btn btn-dark"><span class="material-icons">add</span></i></a>
			<%} %>

			<a href="CartServlet"><i class="btn btn-dark"><span class="material-icons">shopping_cart</span></i></a>

			<%if(isLogin){ %>
			<a href="LogoutServlet"><i class="btn btn-dark"><span class="material-icons">launch</span></i></a>
			<%}else{ %>
			<a href="LoginComfirm"><i class="btn btn-dark"><span class="material-icons">vpn_key</span></i></a>
			<%} %>
		</p>
		</div>
<div>
	 <nav class="navbar navbar-expand-lg navbar-light bg-light">
	 	<div class="collapse navbar-collapse" id="navbarSupportedContent">
	  		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	 			<a class="nav-link" href="Info">INFO</a></li>
	 		<li class="nav-item dropdown">
				<a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
					CATEGORY<span class="caret"></span></a>
			 <div class="dropdown-menu">
				<a class="dropdown-item" href="Category?id=1">青年・少年</a>
				<a class="dropdown-item" href="Category?id=2">恋愛</a>
				<a class="dropdown-item" href="Category?id=3">ファンタジー</a>
				<a class="dropdown-item" href="Category?id=4">その他</a>
			</div>
			</li>
			</ul>
			 <form class="form-inline my-2 my-lg-0">
      		<a class="nav-link my-2 my-sm-0" href="UserInfoDate">INFO-ログ-</a>
    		</form>
		</div>
	</nav>
</div>
		<div>
			<p class="text-center">ユーザ情報</p>

			<div class="container">
				<div class="centering_parent">
				<form action="UserDataUpdateConfirm" method="POST">
				<c:if test="${validationMessage != null}">
				<p class="red-text center-align">${validationMessage}</p>
				</c:if>
					<label for="code" class="control-label col-sm-2">ログインID</label> <input
						type="text" name="login_id" value="${udb.loginId}" class="inline"> <br>

					<label for="name" class="control-label col-sm-2">ユーザ名</label> <input
						type="text" name="user_name" value="${udb.name}" class="inline"> <br>

					<label for="name" class="control-label col-sm-2">住所</label> <input
						type="text" name="user_address" value="${udb.address}" class="inline"> <br>

					<label for="name" class="control-label col-sm-2">パスワード</label> <input
						type="password" name="password" class="inline"> <br>

					<label for="name" class="control-label col-sm-2">ﾊﾟｽﾜｰﾄﾞ(確認)</label> <input
					type="password" name="confirm_password" class="inline" ><br>
					<br>
					<button type="submit" class="btn btn-info">更新する</button>
					<a href="UserDeleteServlet?id=${udb.id}" class="btn btn-danger">削除する</a>
				</form>
				</div>



				<br>
				<div>
					<table class="table">

						<thead>
							<tr>
								<th scope="col"></th>
								<th scope="col">購入日時</th>
								<th scope="col">配送方法</th>
								<th scope="col">合計(税込)</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach var="bdb" items="${Buylist}">
							<tr>
								<th><p class="text-right">
										<a href="UserBuyHistory?buy_id=${bdb.id}"
											class="btn-circle-flat"> <i class="material-icons">details</i></a>
									</p></th>
								<th scope="row">${bdb.formatDate}</th>
								<td>${bdb.deliveryMethodName}</td>
								<td>¥${bdb.formatTotalPrice}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

<div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
				<a href="TopServlet"><i class="btn btn-link">
					<span class="material-icons">arrow_upward</span></i></a></li>
		</ul>
		</div>
	</nav>
</div>






		</div>
	</div>
</body>
</html>
