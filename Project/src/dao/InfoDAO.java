package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.InfoDateBeans;

public class InfoDAO {

	public static ArrayList<InfoDateBeans> getAllInfoDateBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<InfoDateBeans> infoDataBeansList = new ArrayList<InfoDateBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_info ORDER BY t_info.id DESC");

			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				InfoDateBeans infodb = new InfoDateBeans();
				infodb.setId(rs.getInt("id"));
				infodb.setUserId(rs.getString("user_id"));
				infodb.setDetail(rs.getString("detail"));
				infodb.setCreateDate(rs.getString("create_date"));
				infoDataBeansList.add(infodb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
			System.out.println("searching all InfoDataBeans has been completed");

			return infoDataBeansList;
		}


	public static void insertUser(String id, String detail) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();


			st = con.prepareStatement("INSERT INTO t_info(user_id,detail,create_date) VALUES(?,?,?)");
			st.setString(1, id);
			st.setString(2, detail);
			st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));

			st.executeUpdate();

			System.out.println("inserting INFO has been completed");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	public static ArrayList<InfoDateBeans> getUserInfoDateBeans(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		ArrayList<InfoDateBeans> infoList = new ArrayList<InfoDateBeans>();
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_info  WHERE user_id = ? ORDER BY t_info.create_date DESC");
			st.setInt(1,id);
			ResultSet rs = st.executeQuery();


			while (rs.next()) {
				InfoDateBeans infodb = new InfoDateBeans();
				infodb.setId(rs.getInt("id"));
				infodb.setDetail(rs.getString("detail"));
				infodb.setCreateDate(rs.getString("create_date"));
				infoList.add(infodb);
			}


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
			System.out.println("searching all InfoDataBeans has been completed");

			return infoList;
		}


}
