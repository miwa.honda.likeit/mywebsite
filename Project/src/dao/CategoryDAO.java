package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.CategoryBeans;


public class CategoryDAO {

	public static ArrayList<CategoryBeans> getAllCategoryBeans() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM s_category");

			ResultSet rs = st.executeQuery();

			ArrayList<CategoryBeans> categoryBeansList = new ArrayList<CategoryBeans>();
			while (rs.next()) {
				CategoryBeans cbd = new CategoryBeans();
				cbd.setId(rs.getInt("id"));
				cbd.setName(rs.getString("name"));
				categoryBeansList.add(cbd);
			}

			System.out.println("searching all DeliveryMethodDataBeans has been completed");

			return categoryBeansList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}





}
