package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemDataBeans;

/**
 *
 * @author d-yamaguchi
 *
 */
public class ItemDAO {



	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
//	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
//		Connection con = null;
//		PreparedStatement st = null;
//		try {
//			con = DBManager.getConnection();
//
//			st = con.prepareStatement("SELECT * FROM m_item ORDER BY RAND() LIMIT ? ");
//			st.setInt(1, limit);
//
//			ResultSet rs = st.executeQuery();
//
//			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
//
//			while (rs.next()) {
//				ItemDataBeans item = new ItemDataBeans();
//				item.setId(rs.getInt("id"));
//				item.setName(rs.getString("name"));
//				item.setDetail(rs.getString("detail"));
//				item.setPrice(rs.getInt("price"));
//				item.setFileName(rs.getString("file_name"));
//				itemList.add(item);
//			}
//			System.out.println("getAllItem completed");
//			return itemList;
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//			throw new SQLException(e);
//		} finally {
//			if (con != null) {
//				con.close();
//			}
//		}
//	}
	//全ての商品
	public static ArrayList<ItemDataBeans> findAll() {
		Connection conn = null;
		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT *FROM s_item_date;";
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// itemインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setAuthor(rs.getString("author"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setStock(rs.getInt("stock"));
				item.setCategoryId(rs.getInt("category_id"));
				itemList.add(item);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	/**
	 * 商品IDによる商品検索
	 * @param itemId
	 * @return ItemDataBeans
	 * @throws SQLException
	 */
	public static ItemDataBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM s_item_date WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans item = new ItemDataBeans();
			if (rs.next()) {
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				//修正
				item.setAuthor(rs.getString("author"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setStock(rs.getInt("stock"));
				item.setCategoryId(rs.getInt("category_id"));
			}

			System.out.println("searching item by itemID has been completed");

			return item;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemsByItemName(String searchWord, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			//修正
			if (searchWord.length() == 0) {
				//全検索
				st = con.prepareStatement("SELECT * FROM m_item ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			}else{
				//商品名検索
				st = con.prepareStatement("SELECT * FROM m_item WHERE name LIKE ? ORDER BY id ASC LIMIT ?,?");
				//修正
				st.setString(1, "%"+searchWord+"%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCount(String searchWord) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}


	//追加 buyhistory
	/**
	 * buyIDによる購入情報検索
	 * @param buyId
	 * @return BuyDataBeans
	 * 				購入情報のデータを持つJavaBeansの
	 * @throws SQLException
	 * 				呼び出し元にスローさせるため
	 */
//	public static ArrayList<ItemDataBeans> getBuyDetailDataBeansbuyId(String id) throws SQLException {
//		Connection con = null;
//		PreparedStatement st = null;
//		try {
//			con = DBManager.getConnection();
//
//			st = con.prepareStatement(
//					"SELECT m_item.name,m_item.price"
//							+" FROM t_buy_detail"
//							+" JOIN m_item"
//							+" ON t_buy_detail.item_id = m_item.id"
//							+" WHERE t_buy_detail.buy_id = ?");
//			st.setString(1, id);
//			ResultSet rs = st.executeQuery();
//			ArrayList<ItemDataBeans> bdbdiList =new ArrayList<ItemDataBeans>();
//
//			while (rs.next()) {
//				ItemDataBeans item = new ItemDataBeans();
//				item.setName(rs.getString("name"));
//				item.setPrice(rs.getInt("price"));
//				bdbdiList.add(item);
//			}
//
//			System.out.println("searching buyItemDate!!");
//			return bdbdiList;
//
//		} catch (SQLException e) {
//			System.out.println(e.getMessage());
//			throw new SQLException(e);
//		} finally {
//			if (con != null) {
//			try {
//				con.close();
//			} catch (SQLException e) {
//				e.printStackTrace();
//				return null;
//			}
//		}
//		}
//	}
	/**
	 * category別の商品
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemDataBeansListbyC(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
			"SELECT s_item_date.id,"
				+ " s_item_date.name,s_item_date.detail,s_item_date.file_name,"
				+ " s_item_date.price,s_item_date.author"
				+ " FROM s_item_date"
				+ " JOIN s_category"
				+ " ON s_item_date.category_id = s_category.id"
				+ " WHERE s_category.id = ?");
		st.setInt(1, id);

		ResultSet rs = st.executeQuery();
		ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		while (rs.next()) {
			ItemDataBeans item = new ItemDataBeans();
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setAuthor(rs.getString("author"));
			item.setFileName(rs.getString("file_name"));
			itemList.add(item);
		}

		System.out.println("searching ItemDataBeansList by id has been completed");
				return itemList;
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
	/*
	 * 在庫数追加
	 */
	public static void itemAadd(int stock,int id) throws SQLException {
		// 更新された情報をセットされたJavaBeansのリスト
//		ItemDataBeans updatedUdb = new ItemDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE s_item_date SET stock=? WHERE id=?;");
			st.setInt(1,stock);
			st.setInt(2,id);
			st.executeUpdate();
			System.out.println("update stock has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * itemデータの挿入処理を行う
	 *
	 */
	public static void insertItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO s_item_date(name,author,detail,price,file_name,stock,category_id) VALUES(?,?,?,?,?,?,?)");

			st.setString(1, idb.getName());
			st.setString(2, idb.getAuthor());
			st.setString(3, idb.getDetail());
			st.setInt(4, idb.getPrice());
			st.setString(5,idb.getFileName());
			st.setInt(6,idb.getStock());
			st.setInt(7,idb.getCategoryId());

			st.executeUpdate();

			System.out.println("inserting itemDate has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//削除
		public static void itemDelete(String id) {
			Connection con = null;
			try {
				// データベースへ接続
				con = DBManager.getConnection();

				String st = "DELETE FROM s_item_date WHERE id = ?";

				PreparedStatement pStmt = con.prepareStatement(st);
				pStmt.setString(1, id);
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();

					}
				}
			}

		}
}
