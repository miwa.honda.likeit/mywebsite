package beans;

import java.io.Serializable;

/**
 * ユーザー
 * @author d-yamaguchi
 *
 */
public class UserDataBeans implements Serializable {
	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private String createDate;

	// コンストラクタ
	public UserDataBeans() {
		this.name = "";
		this.address = "";
		this.loginId = "";
		this.password = "";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * ユーザー情報更新時の必要情報をまとめてセットするための処理
	 *
	 * @param name
	 * @param loginId
	 * @param address
	 * passwordを追加
	 *
	 */
	public void setUpdateUserDataBeansInfo(String name, String loginId, String address, int id,String password) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}



}
