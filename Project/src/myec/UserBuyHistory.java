package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//追加
		HttpSession session = request.getSession();
		try {
			//共通
			String sid = request.getParameter("buy_id");
			int id = Integer.parseInt(sid);
			//配送
			BuyDataBeans buydb = BuyDAO.getBuyDataBeansByBuyId(id);
			request.setAttribute("buydb", buydb);


			//item
			ArrayList<ItemDataBeans> bdbdiList = BuyDetailDAO.getItemDataBeansListByBuyId(id);
			request.setAttribute("bdbdiList", bdbdiList);

			request.getRequestDispatcher("/WEB-INF/jsp/userdate-s-date.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
}

	private String Integer(String parameter) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

		//追加(終)



	}

