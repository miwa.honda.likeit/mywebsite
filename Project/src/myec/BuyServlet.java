package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class BuyServlet
 */
@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession();
		try {

			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "CartServlet");
				// Login画面にリダイレクト
				response.sendRedirect("LoginComfirm");
//				String inputLoginId =session.getAttribute("loginId")!=null?(String) EcHelper.cutSessionAttribute(session,"loginId"):"";
//				String loginErrorMessage = (String)EcHelper.cutSessionAttribute(session, "loginErrorMessage");
//				int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
//				request.setAttribute("inputLoginId", inputLoginId);
//				request.setAttribute("loginErrorMessage", loginErrorMessage);
//				request.setAttribute("inputDeliveryMethodId",inputDeliveryMethodId);
//				request.getRequestDispatcher("/WEB-INF/jsp/logIn.jsp").forward(request, response);

			} else if (cart.size() == 0) {
				request.setAttribute("cartActionMessage", "購入する商品がありません");
				request.getRequestDispatcher("/WEB-INF/jsp/cart.jsp").forward(request, response);
			} else {
				//選択された配送方法IDを取得
				int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
				//選択されたIDをもとに配送方法Beansを取得
				DeliveryMethodDataBeans userSelectDMB = DeliveryMethodDAO.getDeliveryMethodDataBeansByID(inputDeliveryMethodId);
				ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
				//合計金額
//				追加+1
				int dmdbP = userSelectDMB.getPrice();
				int totalPrice = EcHelper.getTotalItemPrice(cartIDBList);

				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setUserId((int) session.getAttribute("userId"));
//				修正
				bdb.setTotalPrice(totalPrice + dmdbP);
				bdb.setDelivertMethodId(userSelectDMB.getId());
//				追加+1
				bdb.setDeliveryMethodPrice(userSelectDMB.getPrice());
				bdb.setDeliveryMethodName(userSelectDMB.getName());


				//購入確定で利用
				session.setAttribute("bdb", bdb);
				request.getRequestDispatcher("/WEB-INF/jsp/buy.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
