package myec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class Category
 */
@WebServlet("/Category")
public class Category extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Category() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//
//		HttpSession session = request.getSession();
//		try {
//			ArrayList<CategoryBeans> cdbList = CategoryDAO.getAllCategoryBeans();
//			request.setAttribute("cdbList", cdbList);
////			request.getRequestDispatcher("/WEB-INF/jsp/category-1.jsp").forward(request, response);
//		} catch (Exception e) {
//			e.printStackTrace();
//			session.setAttribute("errorMessage", e.toString());
//			response.sendRedirect("Error");
//		}
//
//
//}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
		//選択された商品のIDを型変換し利用
		int id = Integer.parseInt(request.getParameter("id"));
		//対象のアイテム情報を取得
		ArrayList<ItemDataBeans> itemC = ItemDAO.getItemDataBeansListbyC(id);
		//リクエストパラメーターにセット
		request.setAttribute("itemC", itemC);
		request.getRequestDispatcher("/WEB-INF/jsp/category-1.jsp").forward(request, response);

	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
	}

}
