package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.InfoDAO;

/**
 * Servlet implementation class inforesult
 */
@WebServlet("/inforesult")
public class inforesult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public inforesult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			request.setCharacterEncoding("UTF-8");
			HttpSession session = request.getSession();
			try {
				// ログイン時に取得したユーザーIDをセッションから取得
			String detail = request.getParameter("area1");
			String id = request.getParameter("id");
			if(id.equals("")) {
				String sid = "IDなし";
				InfoDAO.insertUser(sid,detail);
				response.sendRedirect("TopServlet");
				//request.getRequestDispatcher("/WEB-INF/jsp/top.jsp").forward(request, response);
			}else {
				InfoDAO.insertUser(id,detail);
				response.sendRedirect("TopServlet");
				//request.getRequestDispatcher("/WEB-INF/jsp/top.jsp").forward(request, response);
			}

			} catch (Exception e) {
				e.printStackTrace();
				session.setAttribute("errorMessage", e.toString());
				response.sendRedirect("Error");
			}
		}
}
