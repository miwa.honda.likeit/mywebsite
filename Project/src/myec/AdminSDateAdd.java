package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class AdminSDateAdd
 */
@WebServlet("/AdminSDateAdd")
public class AdminSDateAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminSDateAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {



			String itemName = request.getParameter("name");
			String detail = request.getParameter("date");
			String sitemPrice = request.getParameter("price");
			String filename = request.getParameter("filename");
			String author = request.getParameter("author");
			String sstock = request.getParameter("stock");
			String scategoryId = request.getParameter("categoryId");



		if (itemName.equals("") || detail.equals("") || sitemPrice.equals("") || filename.equals("")|| author.equals("")|| sstock.equals("")||scategoryId.equals("")){
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.getRequestDispatcher("/WEB-INF/jsp/admin-S-date.jsp").forward(request, response);
			return;
		}else{
			int itemPrice =  Integer.parseInt(sitemPrice);
			int stock =  Integer.parseInt(sstock);
			int categoryId =  Integer.parseInt(scategoryId);

			ItemDataBeans iadd = new ItemDataBeans();
			iadd.setUpdateItemDataBeansInfo(itemName,detail,itemPrice,filename,author,stock,categoryId);
			ItemDAO.insertItem(iadd);

			response.sendRedirect("AdminSDate");
		}
//			request.getRequestDispatcher("/WEB-INF/jsp/admin-userdate.jsp").forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

