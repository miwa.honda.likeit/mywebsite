package myec;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class AuserdateUp
 */
@WebServlet("/AuserdateUp")
public class AuserdateUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuserdateUp() {
        super();
        // TODO Auto-generated constructor stub
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション開始
		HttpSession session = request.getSession();
		try {
			int id = Integer.parseInt(request.getParameter("id"));

			UserDataBeans udb = session.getAttribute("returnUDB") == null ? UserDAO.getUserDataBeansByUserId(id) : (UserDataBeans) EcHelper.cutSessionAttribute(session, "returnUDB");
			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");
			request.setAttribute("validationMessage", validationMessage);
			request.setAttribute("udb", udb);
			request.getRequestDispatcher("/WEB-INF/jsp/admin-userdate-up.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		// セッション開始
		HttpSession session = request.getSession();
		try {
			// 入力フォームから受け取った値をUserDataBeansにセット
			String password = request.getParameter("password");
			String sid = request.getParameter("id");
			int id =  Integer.parseInt(sid);
			UserDAO.AdminupdateUser(password,id);
			response.sendRedirect("TopServlet");

			//request.getRequestDispatcher("/WEB-INF/jsp/admin-userdate.jsp").forward(request, response);

	} catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
	}
	}
}
